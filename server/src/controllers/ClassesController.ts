
import {Request, Response} from "express";
import db from "../database/connection";
import converToMinutes from "../utils/converToMinutes";


interface ScheduleItem {
    week_day: number;
    from: string;
    to: string;
}


export default class ClassesController{

    async index(request: Request, response: Response){
        const filters = request.query;

        if (!filters.week_day || !filters.subject || !filters.time ){
            return response.status(400).json({
                error: "Erro de filtro"
            })
        }

        const timeMinutes = converToMinutes(filters.time as string);

        const classes = await db('classes')
            .whereExists(function (){
                this.select('class_schedule.*')
                .from('class_schedule')
                .whereRaw('`class_schedule`.`classId` == `classes`.`id` ')
                .whereRaw('`class_schedule`.`week_day` == ??', [Number(filters.week_day)])  
                .whereRaw('`class_schedule`.`from` <= ??', [timeMinutes])    
                .whereRaw('`class_schedule`.`to` > ??', [timeMinutes])              
            })
            .where('classes.subject', '=', filters.subject as string)
            .join('users', 'classes.userId','=', 'users.id')
            .select(['classes.*', 'users.*']);

        return response.json(classes);
    }


    async  create (request: Request, response:Response) {
        const {
            name,
            avatar,
            whatsapp,
            bio,
            subject,
            cost, 
            schedule
        } = request.body;
    
        const trx = await db.transaction();
    
        try {
            const userIds = await trx('users').insert({
                name, avatar, whatsapp, bio
            });
        
            const userId = userIds[0];
        
            const classesId = await trx('classes').insert({
                subject, cost, userId
            })
        
            const classId = classesId[0];
        
            const classSchedule = schedule.map((x: ScheduleItem)  => {
                return {
                    "week_day": x.week_day,
                    "from": converToMinutes(x.from),
                    "to": converToMinutes(x.to),
                    classId
                }
            });
            
            await trx('class_schedule').insert(classSchedule);
        
            await trx.commit();
            
            return response.status(201).send("Ok");
        }
        catch(error){
    
            trx.rollback();
    
            return response.status(400).json({
                error: "Error na inserção no db."
            });
        }
    }
}