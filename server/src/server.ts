
import express, { request } from "express";
import routes from "./routes";
import cors from "cors";

const app = express();

app.use(cors());
app.use(express.json()); //entender o JSON

app.use(routes)

//server listen
app.listen(3333);

//get - buscar ou listar informações
//post - criar informação
//put - atualizar informação
//delete - apagar informação


//routes e resources
// app.post("/users", function (request, response){

//     const users = [
//         {"name": "Ze", "idade": "21"},
//         {"name": "Ze", "idade": "21"},
//         {"name": "Ze", "idade": "21"}
//     ] 
    
//     return response.json(users); 
// });


// app.delete("/users/:id", (request, response) => {
//     console.log(request.params)
// });

// app.get("/users", (request, response) => {
//     console.log(request.query)
// });

