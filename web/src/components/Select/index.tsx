import React, {SelectHTMLAttributes} from "react";

import './styles.css'

interface SelectProps extends SelectHTMLAttributes<HTMLSelectElement> {
    label: string;
    name: string;
    options: Array<{
        value: string;
        label:string;
    }>;
}

const Select: React.FunctionComponent<SelectProps> = ({
    label, name, options, ...rest
}) => {
    return (
        <div className="select-block">
            <label htmlFor={name}>{label}</label>
            <select value="" id={name} {...rest}>
                <option value="" disabled selected hidden>Selecione uma Opção</option>
                {options.map(x => {
                    return <option key={x.value} value={x.value}>{x.label}</option>
                })}
            </select>
        </div>
    );
}

export default Select;