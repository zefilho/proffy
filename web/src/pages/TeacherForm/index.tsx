import React, {useState, FormEvent} from "react";
import PageHeader from "../../components/PageHeader";
import {useHistory} from "react-router-dom";

import './styles.css'
import Input from "../../components/Input";

import warningIcon from "../../assets/images/icons/warning.svg";
import TextArea from "../../components/TextArea";
import Select from "../../components/Select";
import api from "../../services/api";

function TeacherForm(){

    const history = useHistory();

    const [name, setName] = useState('');
    const [avatar, setAvatar] = useState('');
    const [bio, setBio] = useState('');
    const [whatsapp, setWhatsapp] = useState('');

    const [subject, setSubject] = useState('');
    const [cost, setCost] = useState('');

    const [scheduleItems, setScheduleItems] = useState([
        {week_day: 0, from:"", to: "" },        
    ]);
    
    function addNewScheduleItem(){

        setScheduleItems([
            ...scheduleItems, 
            {week_day: 0, from:"", to: "" }
        ]);
        
    }

    function handleCreateClass(e: FormEvent){
        e.preventDefault();  
        
        api.post('classes', {
            name,
            avatar,
            whatsapp,
            bio,
            subject,
            cost: Number(cost),
            schedule: scheduleItems
        }).then(() => {
            alert('Cadastro Realizado.');
            
            history.push('/');

        }).catch(() =>{
            alert('Erro no cadastro.')
        });
        
    }
    
    function setScheduleItemValue(index:number, field:string, value:string){
        const updateScheduleItems = scheduleItems.map((item, pos) =>{
            if(index === pos){
                return {
                    ...item, [field]: value
                }
            }

            return item;
        } );
       
        setScheduleItems(updateScheduleItems);
        
    }

    return (
        <div id="page-teacher-form" className="container">
            <PageHeader title="Que incrível que você quer dar aulas."
                description = "O primeiro passo é preencher este formulário de inscrição."/>
        
            <main>
                <form onSubmit={handleCreateClass}>
                    <fieldset>
                        <legend>Seus Dados</legend>

                        <Input name="name" label="Nome Completo"
                            value={name}
                            onChange={(e) => {setName(e.target.value)}}/>
                        
                        <Input name="avatar" label="Avatar"
                        value={avatar}
                        onChange={(e) => {setAvatar(e.target.value)}}/>
                        
                        <Input name="whatsapp" label="Whatsapp"
                        value={whatsapp}
                        onChange={(e) => {setWhatsapp(e.target.value)}}/>
                        
                        <TextArea name="bio" label="Biografia"
                        value={bio}
                        onChange={(e) => {setBio(e.target.value)}}/>
                    
                    </fieldset>

                    <fieldset>
                        <legend>Sobre a aula</legend>

                        <Select name="subject" label="Matéria"
                            value={subject}
                            onChange={(e) => {setSubject(e.target.value)}}
                            options={[
                                { value: 'Artes', label: 'Artes' },
                                { value: 'Matemática', label: 'Matemática' },
                                { value: 'Biologia', label: 'Biologia' },
                            ]} />
                        <Input name="cost" label="Hora Aula"
                        value={cost}
                        onChange={(e) => {setCost(e.target.value)}}/>   
                                        
                    </fieldset>

                    <fieldset>
                        <legend>Horários Disponíveis 
                            <button type="button" onClick={addNewScheduleItem}>
                            + Novo horário
                        </button>
                        </legend>

                        {scheduleItems.map(
                            (item, index) => {
                                return (
                                    <div key={item.week_day} className="schedule-item">
                                        <Select name="week_day" 
                                            onChange={e => setScheduleItemValue(
                                                index, 'week_day', e.target.value
                                            )}
                                            value={item.week_day}
                                            label="Dias da Semana"
                                            options={[
                                                { value: '0', label: 'Domingo' },
                                                { value: '1', label: 'Segunda-feira' },
                                                { value: '2', label: 'Terça-feira' },
                                                { value: '3', label: 'Quarta-feira' },
                                                { value: '4', label: 'Quinta-feira' },
                                                { value: '6', label: 'Sexta-feira' },
                                                { value: '7', label: 'Sábado' }
                                            ]} />

                                        <Input name="from" label="Das" type="time" 
                                        value={item.from}
                                        onChange={e => setScheduleItemValue(
                                            index, 'from', e.target.value
                                        )} />
                                        <Input name="to" label="Até" type="time"
                                        value={item.to}
                                        onChange={e => setScheduleItemValue(
                                            index, 'to', e.target.value
                                        )} />
                                    </div>
                                );
                            }
                        )}

                        

                                        
                    </fieldset>

                    <footer>
                    <p>
                        <img src={warningIcon} alt="Aviso Importante"/>
                        Important! <br/>
                        Preencha todos os dados.
                    </p>
                    <button type="submit">
                        Salvar
                    </button>
                </footer>
                </form>
            </main>
        </div>
    );
}

export default TeacherForm;